import React, { useState, useEffect } from "react";
import {
	Button,
	Form,
	FormGroup,
	Input,
	Container,
	Row,
	Col,
} from "reactstrap";
import {
	Card,
	CardImg,
	CardTitle,
	CardText,
	CardColumns,
	CardSubtitle,
	CardBody,
} from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import { Fade } from "reactstrap";
import ReactDOM from "react-dom";

const MinicapImgForm = (props) => {
	//state
	const [minicapPic, setMinicapPic] = useState(undefined);

	const onChangeHandler = (e) => {
		setMinicapPic(e.target);
		setMinicapPic(e.target.files[0]);
	};

	const onSubmitHandler = (e) => {
		e.preventDefault();
		const formData = new FormData();
		formData.append("upload", minicapPic);
		props.updateImage(formData);
	};

	return (
		<Form className="border p-4 rounded" onSubmit={(e) => onSubmitHandler(e)}>
			<h1 align="center" id="title" style={{ paddingTop: "30px" }}>
				ASK THE FORTUNE COOKIE
			</h1>
			<Row style={{ padding: "25px" }}>
				<Col sm="3" align="center">
					<Card style={{ background: "rgb(96, 229, 247)" }}>
						<CardTitle className="cardTitle">LOVE</CardTitle>
						<Link to="/love">
							<CardImg
								top
								width="25%"
								src="/images/depositphotos_328026278-stock-illustration-cute-fortune-cookie-on-a.jpg"
								alt=""
								type="button"
							/>
						</Link>
						<CardBody>
							<CardSubtitle>Card subtitle</CardSubtitle>
							<CardText>
								Some quick example text to build on the card title and make up
								the bulk of the card's content.
							</CardText>
						</CardBody>
					</Card>
				</Col>

				<Col sm="3" align="center">
					<Card style={{ background: "rgb(96, 229, 247)" }}>
						<CardTitle className="cardTitle">CAREER</CardTitle>
						<Link to="/career">
							<CardImg
								top
								width="25%"
								src="/images/depositphotos_328027464-stock-illustration-cartoon-fortune-cookie-isolated-in.jpg"
								alt=""
								type="button"
							/>
						</Link>
						<CardBody>
							<CardSubtitle>Card subtitle</CardSubtitle>
							<CardText>
								Some quick example text to build on the card title and make up
								the bulk of the card's content.
							</CardText>
						</CardBody>
					</Card>
				</Col>

				<Col sm="3" align="center">
					<Card style={{ background: "rgb(96, 229, 247)" }}>
						<CardTitle className="cardTitle">HEALTH</CardTitle>
						<Link to="/health">
							<CardImg
								top
								width="25%"
								src="/images/depositphotos_334913354-stock-illustration-smiley-nurse-chinese-fortune-cookie.jpg"
								alt=""
								type="button"
							/>
						</Link>
						<CardBody>
							<CardSubtitle>Card subtitle</CardSubtitle>
							<CardText>
								Some quick example text to build on the card title and make up
								the bulk of the card's content.
							</CardText>
						</CardBody>
					</Card>
				</Col>

				<Col sm="3" align="center">
					<Card style={{ background: "rgb(96, 229, 247)" }}>
						<CardTitle className="cardTitle">MONEY</CardTitle>
						<Link to="/money">
							<CardImg
								top
								width="25%"
								src="/images/depositphotos_328025902-stock-illustration-cute-fortune-cookie-character-smiley.jpg"
								alt=""
								type="button"
							/>
						</Link>
						<CardBody>
							<CardSubtitle>Card subtitle</CardSubtitle>
							<CardText>
								Some quick example text to build on the card title and make up
								the bulk of the card's content.
							</CardText>
						</CardBody>
					</Card>
				</Col>
			</Row>
		</Form>
	);
};

export default MinicapImgForm;
