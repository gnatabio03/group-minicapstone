import React, { useState, useEffect } from "react";
import { Button, Form, FormGroup, Label, Input, FormText } from "reactstrap";
import { Link, Redirect } from "react-router-dom";
import axios from "axios";
import Swal from "sweetalert2";

const AddQuoteForm = (props) => {
	console.log(props);
	const [formData, setFormData] = useState({
		category: "",
		description: "",
	});

	// const [disabledBtn, setDisabledBtn] = useState(true);

	const { category, description } = formData;

	const onChangeHandler = (e) => {
		setFormData({
			...formData,
			[e.target.name]: e.target.value,
		});
		console.log(formData);
	};

	const onSubmitHandler = async (e) => {
		e.preventDefault();

		if (!category) {
			console.log("Please select category");
			Swal.fire({
				title: "Error",
				text: "Please select category",
				icon: "error",
				showConfirmationButton: false,
				timer: 3000,
			});
		} else {
			console.log(formData);

			const newQuote = {
				category,
				description,
			};

			try {
				const config = {
					headers: {
						"Content-Type": "application/json",
					},
				};
				const body = JSON.stringify(newQuote);
				const res = await axios.post(
					"http://localhost:4000/quotes",
					body,
					config
				);

				console.log(res);

				Swal.fire({
					title: "Success",
					text: "Successfully added quote!",
					icon: "success",
					showConfirmationButton: false,
					timer: 3000,
				});
			} catch (e) {
				Swal.fire({
					title: "Error",
					text: e.response.data.message,
					icon: "error",
					showConfirmationButton: false,
					timer: 3000,
				});
				console.log(e.response.data.message);
			}
		}
	};

	// useEffect(() => {
	//   if(category !== "" && description !== "" ) {
	//     setDisabledBtn(false)
	//   } else {
	//     setDisabledBtn(true)
	//   }
	// }, [formData])

	return (
		<Form className="container mt-3" onSubmit={(e) => onSubmitHandler(e)}>
			<FormGroup>
				<Label for="category">Category</Label>
				<Input
					type="select"
					name="category"
					id="category"
					onChange={(e) => onChangeHandler(e)}>
					<option value="love">Love</option>
					<option value="career">Career</option>
					<option value="health">Health</option>
					<option value="money">Money</option>
				</Input>
				{/*<Input
          type="select"
          name="category"
          id="category"
          value={category}
          onChange={ e => onChangeHandler(e) }
          //VALIDATION
          required
        />*/}
			</FormGroup>
			<FormGroup>
				<Label for="description">Quote</Label>
				<Input
					type="text"
					name="description"
					id="description"
					value={description}
					onChange={(e) => onChangeHandler(e)}
					//VALIDATION
					maxLength="300"
					//pattern="[a-zA-Z._^%$#!~@,-]+" //ANY CASE, ANY COMBINATION OF ALPHANUMERIC CHARACTERS
					required
				/>
			</FormGroup>

			<Button color="primary" className="btn mb-3 btn-block">
				Add Quote
			</Button>
		</Form>
	);
};

export default AddQuoteForm;
