
import React, { useState } from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
// import "./style.css";

//pages
import AppNavbar from './partials/AppNavbar';
import MinicapPage from './pages/MinicapPage';
import LandingPage from './pages/LandingPage';
import AddQuotePage from './pages/AddQuotePage';
import LovePage from './pages/LovePage';
import CareerPage from './pages/CareerPage';
import HealthPage from './pages/HealthPage';
import MoneyPage from './pages/MoneyPage';
import LoveCookiePage from './pages/LoveCookiePage';
import CareerCookiePage from './pages/CareerCookiePage';
import HealthCookiePage from './pages/HealthCookiePage';
import MoneyCookiePage from './pages/MoneyCookiePage';
import NotFoundPage from './pages/NotFoundPage';

const App = () => { 
	const Load = (props, page)=>{
		switch(page) {
			case "LandingPage": return <LandingPage {...props} />	
			case "MinicapPage": return <MinicapPage {...props} />
			case "AddQuotePage": return <AddQuotePage {...props} />
			case "LovePage": return <LovePage {...props} />
			case "CareerPage": return <CareerPage {...props} />
			case "HealthPage": return <HealthPage {...props} />
			case "MoneyPage": return <MoneyPage {...props} />
			case "LoveCookiePage": return <LoveCookiePage {...props} />
			case "CareerCookiePage": return <CareerCookiePage {...props} />
			case "HealthCookiePage": return <HealthCookiePage {...props} />
			case "MoneyCookiePage": return <MoneyCookiePage {...props} />
			default: return <NotFoundPage/>		
		}
	}
	return (
		<BrowserRouter>
			<AppNavbar/>
			<Switch>
				<Route exact path="/" render={ (props)=> Load(props, "LandingPage") }/>	
				<Route path="/minicap" render={ (props)=> Load(props, "MinicapPage") }/>
				<Route path="/addquote" render={ (props)=> Load(props, "AddQuotePage") }/>
				<Route path="/love" render={ (props)=> Load(props, "LovePage") }/>
				<Route path="/career" render={ (props)=> Load(props, "CareerPage") }/>
				<Route path="/health" render={ (props)=> Load(props, "HealthPage") }/>
				<Route path="/money" render={ (props)=> Load(props, "MoneyPage") }/>
				<Route path="/loveCookie" render={ (props)=> Load(props, "LoveCookiePage") }/>
				<Route path="/careerCookie" render={ (props)=> Load(props, "CareerCookiePage") }/>
				<Route path="/healthCookie" render={ (props)=> Load(props, "HealthCookiePage") }/>
				<Route path="/moneyCookie" render={ (props)=> Load(props, "MoneyCookiePage") }/>
				
				{/* <Route exact path="/" /> */}
				<Route path="*" render={ (props)=> Load(props, "NotFoundPage") }/>
			</Switch>
		</BrowserRouter>
	)
}
export default App
