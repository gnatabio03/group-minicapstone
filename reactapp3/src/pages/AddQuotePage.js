import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import AddQuoteForm from '../forms/AddQuoteForm';
import axios from 'axios';

const AddQuotePage = (props) => {
	return (
		<Container>
			<Row>
				<Col>
					<h1>Add Quote</h1>
				</Col>
			</Row>
			<Row>
				<Col>
					<AddQuoteForm/>
				</Col>
			</Row>
            
		</Container>
		);
}

export default AddQuotePage