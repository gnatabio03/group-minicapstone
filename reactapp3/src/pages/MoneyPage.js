import React from "react";
import { Container, Row, Col, Button } from "reactstrap";
import { Link, Redirect } from "react-router-dom";

const MoneyPage = (props) => {
	return (
		<Container id="container">
			<Row>
				<Col>
					<Button href="/">Back</Button>
					<h1 id="title" style={{ paddingTop: "30px" }}>
						Money Page
					</h1>
					<h4>
						Money can't buy happiness, but it sure does make life easier! Just
						click on a cookie below.
					</h4>
				</Col>
			</Row>
			<Row xs="1" sm="2" md="4">
				<Col className="colImg">
					<Link
						to="/moneyCookie"
						style={{ background: "none", border: "none" }}>
						<img
							id="loveBtn"
							alt=""
							src="https://www.horoscope.com/images-US/games/game-fortune-cookie-1.png"></img>
					</Link>
				</Col>
				<Col className="colImg">
					<Link
						to="/moneyCookie"
						style={{ background: "none", border: "none" }}>
						<img
							id="loveBtn"
							alt=""
							src="https://www.horoscope.com/images-US/games/game-fortune-cookie-1.png"></img>
					</Link>
				</Col>
				<Col className="colImg">
					<Link
						to="/moneyCookie"
						style={{ background: "none", border: "none" }}>
						<img
							id="loveBtn"
							alt=""
							src="https://www.horoscope.com/images-US/games/game-fortune-cookie-1.png"></img>
					</Link>
				</Col>
			</Row>
		</Container>
	);
};

export default MoneyPage;
//test
