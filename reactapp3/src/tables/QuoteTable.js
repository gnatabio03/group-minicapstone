import React from 'react';
import { Table } from 'reactstrap';
import QuoteRow from '../rows/QuoteRow';

const QuoteTable = (props) => {
  console.log(props.quotes)

  let row;

  if(!props.quotes) {
    row = (
      <tr colSpan="3">
        <em>No quotes found...</em>
      </tr>
    )
  } else {
    let i = 0;
    row = (
        props.quotes.map(quote => {
          return <QuoteRow quotes={quote} key={quote._id} index={++i} />  
        })
      )
  }

  // if(!props.quotes) {
  //   row = (
  //     <tr colSpan="3">
  //       <em>No members found...</em>
  //     </tr>
  //   )
  // } else {

  // }

  return (
      <Table>
          <thead>
              <tr>
                  <th>#</th>
                  <th>Category</th>
                  <th>Quotes</th>
              </tr>
          </thead>
          <tbody>
              { row }
          </tbody>
      </Table>
  );
}

export default QuoteTable;