import React from "react";
import {
	Card,
	CardImg,
	CardBody,
	CardTitle,
	CardSubtitle,
	CardText,
	Button,
	Container,
	Row,
	Col,
} from "reactstrap";

const Example = (props) => {
	return (
		<>
			<h1 align="center" id="title" style={{ paddingTop: "30px" }}>
				ASK THE FORTUNE COOKIE
			</h1>
			<Row style={{ padding: "25px" }}>
				<Col sm="3" align="center">
					<Card>
						<CardTitle>LOVE</CardTitle>
						<Button href="/LovePage">
							<CardImg
								top
								width="25%"
								src="https://st3.depositphotos.com/9034578/32802/v/1600/depositphotos_328026278-stock-illustration-cute-fortune-cookie-on-a.jpg"
								alt=""
								type="button"
							/>
						</Button>
						<CardBody>
							<CardSubtitle>Card subtitle</CardSubtitle>
							<CardText>
								Some quick example text to build on the card title and make up
								the bulk of the card's content.
							</CardText>
						</CardBody>
					</Card>
				</Col>

				<Col sm="3" align="center">
					<Card>
						<CardTitle>CAREER</CardTitle>
						<Button href="/CareerPage">
							<CardImg
								top
								width="25%"
								src="https://st3.depositphotos.com/9034578/32802/v/450/depositphotos_328027464-stock-illustration-cartoon-fortune-cookie-isolated-in.jpg"
								alt=""
								type="button"
							/>
						</Button>
						<CardBody>
							<CardSubtitle>Card subtitle</CardSubtitle>
							<CardText>
								Some quick example text to build on the card title and make up
								the bulk of the card's content.
							</CardText>
						</CardBody>
					</Card>
				</Col>

				<Col sm="3" align="center">
					<Card>
						<CardTitle>HEALTH</CardTitle>
						<Button href="/HealthPage">
							<CardImg
								top
								width="25%"
								src="https://st3.depositphotos.com/9034578/33491/v/450/depositphotos_334913354-stock-illustration-smiley-nurse-chinese-fortune-cookie.jpg"
								alt=""
								type="button"
							/>
						</Button>
						<CardBody>
							<CardSubtitle>Card subtitle</CardSubtitle>
							<CardText>
								Some quick example text to build on the card title and make up
								the bulk of the card's content.
							</CardText>
						</CardBody>
					</Card>
				</Col>

				<Col sm="3" align="center">
					<Card>
						<CardTitle>MONEY</CardTitle>
						<Button href="/MoneyPage">
							<CardImg
								top
								width="25%"
								src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcT8wolIZFFPWKzuUuLwtTLQsJKTO3CCTdt1b_cpfdyivS5il3eB"
								alt=""
								type="button"
							/>
						</Button>
						<CardBody>
							<CardSubtitle>Card subtitle</CardSubtitle>
							<CardText>
								Some quick example text to build on the card title and make up
								the bulk of the card's content.
							</CardText>
						</CardBody>
					</Card>
				</Col>
			</Row>
		</>
	);
};

export default Example;
//test
