import React, { Fragment } from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "./style.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

//pages
import Navbar from "./partials/Navbar";
import SignupPage from "./pages/SignupPage";
import LovePage from "./pages/LovePage";
import MoneyPage from "./pages/MoneyPage";
import CareerPage from "./pages/CareerPage";
import HealthPage from "./pages/HealthPage";
import LoveCookiePage from "./Cookies/LoveCookiePage";
import CareerCookiePage from "./Cookies/CareerCookiePage";
import MoneyCookiePage from "./Cookies/MoneyCookiePage";
import HealthCookiePage from "./Cookies/HealthCookiePage";

const root = document.querySelector("#root");

const pageComponent = (
	<Fragment>
		<Router>
			<Route exact path="/">
				<SignupPage />
			</Route>
			<Route path="/LovePage">
				<LovePage />
			</Route>
			<Route path="/LoveCookiePage">
				<LoveCookiePage />
			</Route>
			<Route path="/CareerPage">
				<CareerPage />
			</Route>
			<Route path="/CareerCookiePage">
				<CareerCookiePage />
			</Route>
			<Route path="/MoneyPage">
				<MoneyPage />
			</Route>
			<Route path="/MoneyCookiePage">
				<MoneyCookiePage />
			</Route>
			<Route path="/HealthPage">
				<HealthPage />
			</Route>
			<Route path="/HealthCookiePage">
				<HealthCookiePage />
			</Route>
		</Router>
	</Fragment>
);

ReactDOM.render(pageComponent, root);
//test
