import React from "react";
import { Container, Row, Col, Button } from "reactstrap";

const LoveCookiePage = (props) => {
	return (
		<>
			<h1>Love Cookie Page</h1>

			<div class="container">
				<img
					id="loveCookieImg"
					style={{ width: "80%" }}
					alt=""
					src="https://img.clipartlook.com/fortune-cookies-png-dixie-fortune-cookie-clip-art-640_456.png"></img>
				<div class="text-block centered" style={{ background: "white" }}>
					<p>MAGKAKABALIKAN KAYO!</p>
				</div>
				<div class="centered2" style={{ background: "white" }}></div>
			</div>
		</>
	);
};

export default LoveCookiePage;
//testmymumu
