import React, { useState } from 'react';
import {
  Button,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
} from 'reactstrap';

const Example = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <div className="mb-5">
      <Navbar color="dark" light expand="md">
        <NavbarBrand className="text-white" class="font-weight-bold" href="/">MERN Tracker</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              <NavLink className="text-muted" href="/components/Members">Members</NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="text-muted" href="/components/Teams">Teams</NavLink>
            </NavItem>
            <NavItem>
              <NavLink className="text-muted" href="/components/Tasks">Tasks</NavLink>
            </NavItem>
          </Nav>
              <Button color="primary" href="/components/Login">Login</Button>{' '}
              <NavLink className="text-muted" href="">Profile</NavLink>
              <Button color="secondary">Logout</Button>{' '}
        </Collapse>
      </Navbar>
    </div>
  );
}

export default Example;