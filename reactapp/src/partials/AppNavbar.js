import React, { useState, Fragment} from 'react';
import { Link } from 'react-router-dom';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText
} from 'reactstrap';

  const AppNavbar = (props) => {

    return (
        <div className="mb-5">

<Navbar color="dark"  expand="md">
        <NavbarBrand className="font-weight-bold" href="/">Fortune Cookie App</NavbarBrand>
        <NavbarToggler />
        <Collapse navbar>
          <Nav className="mr-auto" navbar>
            <NavItem>
              {/*<NavLink href="/components/">Members</NavLink>*/}
              <Link to="/minicap" className="nav-link">Minicap</Link>
            </NavItem>
            <NavItem>
              {/*<NavLink href="https://github.com/reactstrap/reactstrap">Teams</NavLink>*/}
              <Link to="/addquote" className="nav-link">Add Quotes</Link>
            </NavItem>
            
          </Nav>
          
        
        </Collapse>
      </Navbar>
        </div>
    );
  }

export default AppNavbar;