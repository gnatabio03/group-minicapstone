
import React, { useState } from 'react'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
// import "./style.css";

//pages
import AppNavbar from './partials/AppNavbar';
import MinicapPage from './pages/MinicapPage';
import LandingPage from './pages/LandingPage';
import AddQuotePage from './pages/AddQuotePage';
import NotFoundPage from './pages/NotFoundPage';

const App = () => { 
	const Load = (props, page)=>{
		switch(page) {
			case "LandingPage": return <LandingPage {...props} />	
			case "MinicapPage": return <MinicapPage {...props} />
			case "AddQuotePage": return <AddQuotePage {...props} />
			default: return <NotFoundPage/>		
		}
	}
	return (
		<BrowserRouter>
			<AppNavbar/>
			<Switch>
				<Route exact path="/" render={ (props)=> Load(props, "LandingPage") }/>	
				<Route path="/minicap" render={ (props)=> Load(props, "MinicapPage") }/>
				<Route path="/addquote" render={ (props)=> Load(props, "AddQuotePage") }/>
				{/* <Route exact path="/" /> */}
				<Route path="*" render={ (props)=> Load(props, "NotFoundPage") }/>
			</Switch>
		</BrowserRouter>
	)
}
export default App
