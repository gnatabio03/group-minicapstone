import React, { useState, useEffect } from 'react';
import { Container, Row, Col, Button } from 'reactstrap';
import MinicapImgForm from '../forms/MinicapImgForm';
import axios from 'axios';

const MinicapPage = (props) => {
	return (
		<Container>
			<Row>
				<Col>
					<h1>Fortune</h1>
					<div id="categories">
						<Button className="btn-lg border mr-1">
							LOVE
						</Button>
						<Button className="btn-lg border mr-1">
							CAREER
						</Button>
						<Button className="btn-lg border mr-1">
							MONEY
						</Button>
						<Button className="btn-lg border mr-1">
							HEALTH
						</Button>
						<Button className="btn-lg border mr-1">
							FAMILY
						</Button>
					</div>
				</Col>
				
			</Row>
			<Row>
				<Col>
					<MinicapImgForm/>
				</Col>
			</Row>
            
		</Container>
		);
}

export default MinicapPage