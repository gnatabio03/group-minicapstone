import React, { useState, useEffect } from 'react';
import { Container, Row, Col } from 'reactstrap';
// import QuoteForm from '../forms/QuoteForm';
import QuoteTable from '../tables/QuoteTable';
import axios from 'axios';

const LandingPage = (props) => {
	// console.log("Teamspage props", props.tokenAttr)

	// const [ teamsData, setTeamsData ] = useState({
	// 	token: props.tokenAttr,
	// 	teams: []
	// })

	// const { token, teams } = teamsData;
	// console.log("Teams Component", teams)

	// const getTeams = async () => {
	// 	try {
	// 		const config = {
	// 			headers : {
	// 				Authorization : `Bearer ${token}`
	// 			}

	// 		}

	// 		const res = await axios.get("http://localhost:4000/teams", config)
			
	// 		console.log("Teams Res", res)
	// 		setTeamsData({
	// 			teams: res.data
	// 		})

	// 	} catch(e) {
	// 		console.log(e.response)
	// 		//swal
	// 	}
	// }

	//getTeams()
	// useEffect(()=> {
	// 	getTeams()
	// }, [setTeamsData])

	return (
		<Container className="my-5">
			<Row className="mb-3">
				<Col>
					<h1>Landing page/Quotes Page</h1>
				</Col>
			</Row>
			<Row>
		        {/* <Col md="4">
		        	<TeamForm/>
		        </Col> */}
		        <Col>
		       		<QuoteTable/>
		        </Col>
	      </Row>
		</Container>
		)
}

export default LandingPage;