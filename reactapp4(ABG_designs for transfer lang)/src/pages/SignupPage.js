import React from "react";
// import loveCookieImg from "./images/depositphotos_328026278-stock-illustration-cute-fortune-cookie-on-a.jpg";
import {
	Card,
	CardImg,
	CardBody,
	CardTitle,
	CardSubtitle,
	CardText,
	Button,
	Container,
	Row,
	Col,
} from "reactstrap";

const SignupPage = (props) => {
	return (
		<>
			<h1 align="center" id="title" style={{ paddingTop: "30px" }}>
				ASK THE FORTUNE COOKIE
			</h1>
			<Row style={{ padding: "25px" }}>
				<Col sm="3" align="center">
					<Card style={{ background: "rgb(96, 229, 247)" }}>
						<CardTitle className="cardTitle">LOVE</CardTitle>
						<Button href="/LovePage">
							<CardImg
								top
								width="25%"
								src="/images/depositphotos_328026278-stock-illustration-cute-fortune-cookie-on-a.jpg"
								alt=""
								type="button"
							/>
						</Button>
						<CardBody>
							<CardSubtitle></CardSubtitle>
							<CardText>
								What good fortune lies ahead for you and your special someone?.
								<br></br>
								<br></br>
								<br></br>
							</CardText>
						</CardBody>
					</Card>
				</Col>

				<Col sm="3" align="center">
					<Card style={{ background: "rgb(96, 229, 247)" }}>
						<CardTitle className="cardTitle">CAREER</CardTitle>
						<Button href="/CareerPage">
							<CardImg
								top
								width="25%"
								src="/images/depositphotos_328027464-stock-illustration-cartoon-fortune-cookie-isolated-in.jpg"
								alt=""
								type="button"
							/>
						</Button>
						<CardBody>
							<CardSubtitle></CardSubtitle>
							<CardText>
								Whether you're looking for work, going after a raise or
								promotion find out where your professional life is headed today!
								<br></br>
							</CardText>
						</CardBody>
					</Card>
				</Col>

				<Col sm="3" align="center">
					<Card style={{ background: "rgb(96, 229, 247)" }}>
						<CardTitle className="cardTitle">HEALTH</CardTitle>
						<Button href="/HealthPage">
							<CardImg
								top
								width="25%"
								src="/images/depositphotos_334913354-stock-illustration-smiley-nurse-chinese-fortune-cookie.jpg"
								alt=""
								type="button"
							/>
						</Button>
						<CardBody>
							<CardSubtitle></CardSubtitle>
							<CardText>
								Learn about your personal health and get advice on how to feel
								and look your best today!
								<br></br>
								<br></br>
							</CardText>
						</CardBody>
					</Card>
				</Col>

				<Col sm="3" align="center">
					<Card style={{ background: "rgb(96, 229, 247)" }}>
						<CardTitle className="cardTitle">MONEY</CardTitle>
						<Button href="/MoneyPage">
							<CardImg
								top
								width="25%"
								src="/images/depositphotos_328025902-stock-illustration-cute-fortune-cookie-character-smiley.jpg"
								alt=""
								type="button"
							/>
						</Button>
						<CardBody>
							<CardSubtitle></CardSubtitle>
							<CardText>
								Money can't buy happiness, but it sure does make life easier!
								<br></br>
								<br></br>
								<br></br>
							</CardText>
						</CardBody>
					</Card>
				</Col>
			</Row>
		</>
	);
};

export default SignupPage;
//test
